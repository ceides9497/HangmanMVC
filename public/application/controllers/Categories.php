<?php
class Categories extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Category_model');
	}

	public function index()
	{
		$data['categories'] = $this->Category_model->getCategories();

		$this->load->view("categories/index", $data);
 	}

	public function create()
	{
		$newCategoryName = $this->input->post('newCategoryTextBox');

		$lastId = $this->Category_model->createCategory($newCategoryName);

		$this->load->helper('url');
		redirect('/categories/');
	}


public function delete()
{
	$categoryId = $this->uri->segment(3);
	$this->Category_model->deleteCategory($categoryId);

	$this->load->helper('url');
	redirect('/categories');
}


	public function show()
	{

		$categoryId = $this->uri->segment(3);
		$data['words'] = $this->Category_model->getShow($categoryId);
		$data['info']= $this->Category_model->getInfoById($categoryId);
		$this->load->view("/categories/show", $data);
	}
}
?>
