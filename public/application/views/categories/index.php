<!DOCTYPE html>
<html lang="es-BO">
  <head>
    <title>El ahorcado - Categorías</title>
	<meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/admin.css">
  </head>
  <body>

    <h1>Categorías</h1>
	<ul>
	  <?php foreach ($categories as $category): ?>
	  <li><a href="/categories/show/<?= $category["id"]?>"><?= $category["name"] ?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	<a  class="btn btn-danger" href ="/categories/delete/<?= $category["id"]?>">Borrar</a> </li>
	  <?php endforeach; ?>
	  <li>
		<form method="POST" action="/categories/create">
		  <input type="text" name="newCategoryTextBox" maxlength="50" />
		  <input type="submit" name="createNewCategoryButton" value="Crear" />
		</form>
	  </li>
	</ul>
  </body>
</html>
