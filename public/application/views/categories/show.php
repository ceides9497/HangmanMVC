<!DOCTYPE html>
<html lang="es-BO">
  <head>
    <title>El ahorcado - Categoria</title>
	<meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/admin.css">
  </head>
  <body>

    <h1>Palabras de <?php echo $info['name']?></h1>
	<ul>
	  <?php foreach ($words as $word): ?>
	  <li>
	  	<?= $word["text"] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  	<a  href ="/words/delete/<?= $word["id"]?>/<?= $word["categoryId"]?>">Borrar</a>
	  </li>
	  <?php endforeach; ?>
	</ul>
	<form  method="POST" action="/words/create">
		  <input type="text" name="newWordTextBox" maxlength="50" />
		  <input hidden="true" type="text" name="categoryId" value= <?php echo $info['id']?> maxlength="50" />
		  <input type="submit" name="createNewWordButton" value="Agregar Palabra" />
	</form>
  </body>
</html>
